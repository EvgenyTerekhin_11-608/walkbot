namespace FMS.Infrastructure.Infrastructure
{
    public class Place
    {
        public string name = "";
        public float location_lat = (float) 0;
        public float location_lng = (float) 0;
        public float rating = (float) 0;
        public string address = "";
    }
}