using System.Collections.Generic;
using FMS.Infrastructure.Infrastructure.UserService;

namespace FMS.Infrastructure.Infrastructure.PermissionFilter
{
    public abstract class PermissionFilter<TEntity> : IPermissionFilter<TEntity>
    {
        private readonly IUserService _userService;

        public PermissionFilter(IUserService userService)
        {
            _userService = userService;
        }

        public abstract IEnumerable<TEntity> Filter(IEnumerable<TEntity> infoStorage);
    }
}