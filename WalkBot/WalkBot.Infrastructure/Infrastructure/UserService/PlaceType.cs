using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public static class PlaceTypeExtensions
    {
        public static IEnumerable<string> GetFilter(this PlaceType placeType)
        {
            return typeof(PlaceType).GetMember(placeType.ToString()).First().GetCustomAttribute<ConvertToAttribute>()
                .GetFilterType();
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property |
                    AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class FieldNameAttribute : Attribute
    {
        public string Name { get; set; }

        public FieldNameAttribute(string name)
        {
            Name = name;
        }
    }

    public class ConvertToAttribute : Attribute
    {
        readonly FilterType _ft;

        public ConvertToAttribute(FilterType ft)
        {
            _ft = ft;
        }

        public IEnumerable<string> GetFilterType()
        {
            var strings = _ft.ToString().Split(", ");
            var memberInfos = typeof(FilterType).GetMembers().Where(x => strings.Contains(x.Name)).ToList();
            return memberInfos.Select(x => x.GetCustomAttribute<FieldNameAttribute>().Name).ToList();
        }
    }

    [Flags]
    public enum FilterType
    {
        [FieldName("is_amusement_park")] [Display(Name = "Парк развлечений")]
        AmusementType = 1,

        [Display(Name = "Океанариум")] [FieldName("is_aquarium")]
        Aquarium = 2,

        [Display(Name = "Картинная галерея")] [FieldName("is_art_gallery")]
        ArtGallery = 4,

        [Display(Name = "Боулинг")] [FieldName("is_bowling_alley")]
        BowlingAlley = 8,

        [Display(Name = "Библиотека")] [FieldName("is_library")]
        Library = 16,

        [Display(Name = "Кинотеатр")] [FieldName("is_movie_theater")]
        MovieTheater = 32,

        [Display(Name = "Кафе")] [FieldName("is_cafe")]
        Cafe = 128,

        [Display(Name = "Ночной клуб")] [FieldName("is_night_club")]
        NightClub = 512,

        [Display(Name = "Парк")] [FieldName("is_park")]
        Park = 1024,

        [Display(Name = "Музей")] [FieldName("is_museum")]
        Museum = 2048,

        [Display(Name = "Ресторан")] [FieldName("is_restaurant")]
        Restaraunt = 4096,

        [Display(Name = "Зоопарк")] [FieldName("is_zoo")]
        Zoo = 8192
    }

    public enum PlaceType
    {
        [ConvertTo(FilterType.Cafe | FilterType.Restaraunt)] [Display(Name = "Перекус")]
        Snack,

        [ConvertTo(FilterType.Aquarium | FilterType.Library | FilterType.Park |
                   FilterType.Zoo | FilterType.ArtGallery | FilterType.MovieTheater)]
        [Display(Name = "Культурный отдых")]
        Culture,

        [ConvertTo(FilterType.BowlingAlley | FilterType.Park)] [Display(Name = "Спортивный отдых")]
        Sport,

        [ConvertTo(FilterType.AmusementType | FilterType.NightClub)] [Display(Name = "Развлечения")]
        Entertainment
    }
}