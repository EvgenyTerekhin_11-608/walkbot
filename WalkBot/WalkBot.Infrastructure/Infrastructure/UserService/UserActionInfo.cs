using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using FMS.GSheets.GSheets.State;
using Newtonsoft.Json;

namespace FMS.Infrastructure.Infrastructure.UserService
{
    public class UserActionInfoStorage : Storage.Storage
    {
        public UserInfo UserInfo { get; set; }
        public CurrentStateInfo CurrentStateInfo { get; set; } = new CurrentStateInfo();

        public void AddChatId(long chatId)
        {
            UserInfo.SetChatId(chatId);
        }

        public long ChatId() => UserInfo.ChatId;
    }

    public class FilterTypeInfo
    {
        public int Id { get; set; }
        public FilterType Type { get; set; }
    }
    
    public class UserStateInfo
    {
        public int Id { get; set; }
        public PartnerType PartnerType { get; set; }
        public Age Age { get; set; }
        public RelaxationType RelaxationType { get; set; }
        public PlaceType PlaceType { get; set; }
        public string Localization { get; set; }

        public List<FilterTypeInfo> FilterType { get; set; }

        public Cost Cost { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public UserStateInfo()
        {
            FilterType = new List<FilterTypeInfo>();
        }
    }

    public class UserInfo
    {
        public int UserId { get; set; }
        public StateType Type { get; protected set; } = StateType.UnAuth;
        public IEnumerable<UserStateInfo> Statistic { get; protected set; }
        public UserStateInfo CurrentState { get; protected set; }
        public string UserName { get; }
        public long ChatId { get; private set; }

        public UserInfo(long chatId)
        {
            ChatId = chatId;
            CurrentState = new UserStateInfo();
            Statistic = new List<UserStateInfo>();
        }

        protected UserInfo()
        {
        }

        public UserInfo(long chatId, string userName) : this(chatId)
        {
            UserName = userName;
        }

        public void SetChatId(long chatId)
        {
            ChatId = chatId;
        }
    }

    public class CurrentStateInfo
    {
        public StateType Type { get; private set; } = StateType.UnAuth;

        public void ChangeState(StateType newState)
        {
            Type = newState;
            Console.WriteLine(newState);
        }
    }
}