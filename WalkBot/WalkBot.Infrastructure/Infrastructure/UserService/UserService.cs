namespace FMS.Infrastructure.Infrastructure.UserService
{
    public class UserService : IUserService
    {
        public long ChatId { get; set; }
        public string Username { get; set; }
        
        public UserService(long chatId, string username = "")
        {
            Username = username;
            ChatId = chatId;
        }

        string IUserService.Username()
        {
            return Username;
        }

        long IUserService.ChatId()
        {
            return ChatId;
        }
    }
}