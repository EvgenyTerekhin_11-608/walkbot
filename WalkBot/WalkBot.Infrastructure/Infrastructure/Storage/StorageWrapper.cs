using System.Collections.Generic;
using FMS.Infrastructure.Extensions;
using FMS.Infrastructure.Infrastructure.UserService;

namespace FMS.Infrastructure.Infrastructure.Storage
{
    public class StorageWrapper<TStorage> where TStorage : Storage
    {
        protected readonly IUserService UserService;
        protected TStorage Storage { get; }
        
        public TStorage GetStorage => Storage;

        public StorageWrapper(TStorage storage, IUserService userService)
        {
            UserService = userService;
            Storage = storage;
        }

        public T Set<T>()
        {
            return Storage.Set<TStorage, T>();
        }

        public T Set<T>(string name) where T : class
        {
            return Storage.Set<TStorage, T>(name);
        }
    }

    public interface IImutableValueWrapper
    {
        IEnumerable<T> SetCollection<T>() where T : class;
    }

    public interface IMutableValueWrapper
    {
        T Set<T>() where T : class;
        T Set<T>(string name) where T : class;
        List<T> SetCollection<T>() where T : class;
    }
}