using System.Collections.Generic;
using System.Linq;
using FMS.GSheets.GSheets;
using FMS.Infrastructure.Extensions;
using FMS.Infrastructure.Infrastructure.Storage;
using FMS.Infrastructure.Infrastructure.UserService;

namespace FMS.Infrastructure.Infrastructure
{
    public class UsersInformation: Storage.Storage
    {
        private Dictionary<long, UserActionInfoStorage> UserInformation { get; set; }

        public UsersInformation()
        {
            UserInformation = new Dictionary<long, UserActionInfoStorage>();
        }

        public void AddUser(long chatId)
        {
            UserInformation.Add(chatId, new UserActionInfoStorage());
        }

        public bool ContainsKey(long chatId)
        {
            return UserInformation.ContainsKey(chatId);
        }
        
        public UserActionInfoStorage GetData(long chatId)
        { 
            return UserInformation[chatId];
        }
    }
}