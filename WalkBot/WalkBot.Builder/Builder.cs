using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using FMS.Builder.Cache;

namespace FMS.Builder
{
    public class Builder
    {
        private RegisterAnalyzer Analyzer { get; set; }
        private AssemblyInfo AssemblyInfo { get; set; }

        private Cache<Type, object> _entityCache = new Cache<Type, object>();

        public Builder()
        {
            Analyzer = new RegisterAnalyzer();
            AssemblyInfo = new AssemblyInfo();
        }

        //        types are needed for several constructors, will be added later 
        public Tcast Build<Tcast>(IEnumerable<object> typeValues)
        {
            var res = Build(typeof(Tcast), typeValues);
            return (Tcast) res;
        }

        public object Build(string typeName, IEnumerable<object> typeValues)
        {
            var result = Build(AssemblyInfo.GetTypeByName(typeName, false), typeValues);
            return result;
        }

        public object Build(Type type, IEnumerable<object> typeValues)
        {
            return _entityCache.GetOrDo(type, t =>
            {
                var contructorParameters = type.GetConstructors().OrderBy(x => x.GetParameters().Count()).First()
                    .GetParameters()
                    .Select(x => x.ParameterType);
                var expressionCtorParameters = contructorParameters.Select(Expression.Parameter).ToList();
                var missingParameters =
                    contructorParameters.Where(x =>
                    {
                        if (x.IsInterface)
                        {    
                            return AppDomain.CurrentDomain
                                .GetAssemblies()
                                .Where(xx => !xx.IsDynamic)
                                .SelectMany(xx => xx.GetExportedTypes())
                                .Where(xx => xx.GetInterfaces().Contains(x))
                                .Any(xx=>typeValues.Contains(x));
                        }

                        return !typeValues.Select(xx => xx.GetType()).Contains(x);
                    });
                var createdmissingParameters = missingParameters.Select(DeepBuild).ToList();
                var valueslist = typeValues.ToList();
                valueslist.AddRange(createdmissingParameters);
                var orderTypeValues = valueslist.OrderBy(x => x.GetType(), new TypeComparer(contructorParameters));

                var constructor = type.GetConstructor(
                    BindingFlags.Instance | BindingFlags.Public,
                    null,
                    CallingConventions.HasThis,
                    contructorParameters.ToArray(),
                    new ParameterModifier[0]);

                var expressonCtor = Expression.New(constructor, expressionCtorParameters);
                var constructorCallingLambda = Expression
                    .Lambda(expressonCtor, expressionCtorParameters)
                    .Compile();

                var res = constructorCallingLambda.DynamicInvoke(orderTypeValues.ToArray());
                return res;
            });
        }

        private Tcast Build<Tcast>()
        {
            var res = Build(typeof(Tcast));
            return (Tcast) res;
        }

        private object Build(Type type)
        {
            return _entityCache.GetOrDo(type, t =>
            {
                var constructor = type.GetConstructor(
                    BindingFlags.Instance | BindingFlags.Public,
                    null,
                    CallingConventions.HasThis,
                    new Type[] { },
                    new ParameterModifier[0]);
                var expressonCtor = Expression.New(constructor);

                var constructorCallingLambda = Expression
                    .Lambda<Func<object>>(expressonCtor)
                    .Compile();
                var result = constructorCallingLambda();
                return result;
            });
        }

        public Tcast DeepBuild<Tcast>()
        {
            return (Tcast) _entityCache.GetOrDo(typeof(Tcast), t => RecBuilder(typeof(Tcast).FullName));
        }

        public object DeepBuild(Type type)
        {
            return _entityCache.GetOrDo(type, t => RecBuilder(type.FullName));
        }

        private object RecBuilder(string type)
        {
            var list = Analyzer.ParamsServices(type);
            var services = new List<object>();
            foreach (var t in list)
            {
                var paramServices = Analyzer.ParamsServices(t);
                if (paramServices.Count == 0)
                {
                    services.Add(Build(AssemblyInfo.GetTypeByName(t)));
                    continue;
                }

                var paramServicesValue = paramServices.Select(RecBuilder);
                var paramServicesTypes = AssemblyInfo.GetTypeByName(paramServices);
                var res = Build(AssemblyInfo.GetTypeByName(t), paramServicesValue);
                services.Add(res);
            }

            if (services.Count == 0)
                return Build(AssemblyInfo.GetTypeByName(type));


            return Build(AssemblyInfo.GetTypeByName(type), services);
        }
    }
}