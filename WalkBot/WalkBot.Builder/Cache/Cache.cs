using System;
using System.Collections.Concurrent;

namespace FMS.Builder.Cache
{
    public class Cache<TKey, TValue>
    {
        private ConcurrentDictionary<TKey, TValue> _cache = new ConcurrentDictionary<TKey, TValue>();

        public void Add(TKey key, TValue value)
        {
            _cache.TryAdd(key, value);
        }

        public TValue TryGetValue(TKey key)
        {
            _cache.TryGetValue(key, out var value);
            return value;
        }

        public TValue GetOrDo(TKey key, Func<TKey, TValue> func)
        {
            return func(key);
            
            // Кэш кэширует Sender, поэтому пока отключен
            
            /*if (typeof(TKey) == typeof(Sender) || typeof(TKey) == typeof(ISender) )
                func(key);
            var founded = cache.TryGetValue(key, out var value);
            if (founded) return value;
            value = func(key);
            Add(key, value);
            return value;*/
        }
    }
}