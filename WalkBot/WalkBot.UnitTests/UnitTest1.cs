using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;
using NUnit.Framework;
using WalkBot.TelegramBot.TelegramBot.ISender;

namespace UnitTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var s = new EnumFilter().GetHidden<RelaxationType, PlaceType>(RelaxationType.Passive);
            Assert.IsNotEmpty(s);
        }

        [Test]
        public void Test2()
        {
            var s = new EnumFilter().GetHidden<RelaxationType, RelaxationType>(RelaxationType.Passive);
            Assert.IsEmpty(s);
        }

        [Test]
        public void Test3()
        {
            var s = new EnumFilter().GetHidden<RelaxationType, PartnerType, PlaceType>(
                RelaxationType.Passive, PartnerType.Friends);
            Assert.IsNotEmpty(s);
        }
        
        [Test]
        public void Test4()
        {
            var f = (object)FilterType.Cafe;
            var type = f.GetType();
        }
    }
}