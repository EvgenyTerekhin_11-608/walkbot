using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.Extensions
{
    public static class UpdateExtention
    {
        public static string GetMessage(this Update update)
        {
            var message = update.Message?.Text ??
                          update.Message?.Caption ?? 
                          update.CallbackQuery?.Data;
            message = message?.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
            
            return message;
        }

        public static string GetUsername(this Update update)
        {
            return update.Message?.From?.Username ??
                   update.CallbackQuery?.From?.Username ??
                   update.EditedMessage?.From?.Username;
        }

        public static int GetChatId(this Update update)
        {
            return update.Message?.From?.Id ??
                   update.CallbackQuery?.From?.Id ??
                   update.EditedMessage?.From?.Id ??
                   0;
        }
       
    }
}