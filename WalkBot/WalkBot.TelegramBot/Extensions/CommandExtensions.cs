using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;
using Google.Apis.Util;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.Extensions
{
    public static class CommandExtensions
    {
        public static InlineKeyboardMarkup AddBack(this Command command, List<InlineKeyboardButton> buttons)
        {
            int teil;
            if (buttons.Count % 2 == 0)
                teil = 2;
            else
                teil = 3;
            buttons.Add("Назад");

            return new InlineKeyboardMarkup(buttons.ToTeil(teil));
        }

        public static ReplyKeyboardMarkup AddBackReply(this Command command, List<KeyboardButton> buttons)
        {
            buttons.Add("Назад");

            return new ReplyKeyboardMarkup(buttons.ToTeil(1), true, true);
        }

        public static IEnumerable<InlineKeyboardButton> EnumFilter<T>(
            this IEnumerable<InlineKeyboardButton> list, IEnumerable<T> hidden)
        {
            return list.Where(x => !hidden.Contains((T) (object) int.Parse(x.CallbackData)));
        }

        public static IEnumerable<string> EnumDisplayNames(this Type enumType)
        {
            var buttons1 = enumType.GetMembers()
                .Select(x => x.GetCustomAttribute<DisplayAttribute>())
                .Where(x => x != null)
                .Select(x => x.Name).ToList();

            return buttons1;
        }

        public static IEnumerable<IEnumerable<InlineKeyboardButton>> Format<EnumType>(this IEnumerable<EnumType> hidden)
        {
            var enumerable = typeof(EnumType).GetMembers()
                .Select(x => x.GetCustomAttribute<DisplayAttribute>())
                .Where(x => x != null)
                .Select(x => x.Name)
                .Select((x, i) =>
                {
                    var values = Enum.GetValues(typeof(EnumType));
                    var value = ((int)values.GetValue(i)).ToString();
                    return new InlineKeyboardButton {Text = x, CallbackData = value};
                })
                .EnumFilter(hidden)
                .ToTeil(2);
            return enumerable;
        }

        public static string GetDisplayEnum(this object t)
        {
            var memberInfos = t.GetType().GetMembers().ToList();
            var name = memberInfos.First(x => x.Name == t.ToString())
                .GetCustomAttribute<FieldNameAttribute>().Name;
            return name;
        }
        
        public static string GetDisplayNormal(this object t)
        {
            var memberInfos = t.GetType().GetMembers().ToList();
            var name = memberInfos.First(x => x.Name == t.ToString())
                .GetCustomAttribute<DisplayAttribute>().Name;
            return name;
        }
    }
}