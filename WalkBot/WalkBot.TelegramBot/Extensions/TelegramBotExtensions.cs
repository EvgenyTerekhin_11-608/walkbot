using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace WalkBot.TelegramBot.Extensions
{
    public static class TelegramBotExtensions
    {
        public static async Task<bool> SendMessage(this TelegramBotClient client, SenderMessage message)
        {
            if (message.Latitude != 0 && message.Longitude != 0)
            {
                try
                {
                    await client.SendLocationAsync(message.ChatId, message.Latitude, message.Longitude, replyMarkup: message.Buttons);
                }
                catch (Exception)
                {
                    // Do nothing
                }
            }
            
            if (String.IsNullOrEmpty(message.Message))
            {
                try
                {
                    await client.EditMessageReplyMarkupAsync(
                        message.ChatId,
                        message.MessageId,
                        (InlineKeyboardMarkup) message.Buttons);
                }
                catch (Exception)
                {
                    // Do nothing
                }
            }
            
            
            if (message.MessageId != 0)
            {
                try
                {
                    await client.EditMessageTextAsync(
                        message.ChatId,
                        message.MessageId,
                        message.Message,
                        replyMarkup: (InlineKeyboardMarkup) message.Buttons,
                        parseMode: ParseMode.Html,
                        disableWebPagePreview: true);
                    return true;
                }
                catch (Exception)
                {
                    // Do nothing
                }
            }
            
            try
            {
                await client.SendTextMessageAsync(
                    message.ChatId,
                    message.Message,
                    replyToMessageId: message.ReplyToMessageId,
                    replyMarkup: message.Buttons,
                    parseMode: ParseMode.Html,
                    disableWebPagePreview:true,
                    disableNotification: !message.SendNotification);
                return true;
            }
            catch (Exception)
            {
                // do nothing
            }
            return false;
        }
    }
    
    public class SenderMessage
    {
        public long ChatId { get; set; }
        public string Message { get; set; }
        public int MessageId { get; set; }
        public IReplyMarkup Buttons { get; set; }
        public bool SendNotification { get; set; }      
        public int ReplyToMessageId { get; set; }
        
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public SenderMessage(long id, float latitude, float longitude, IReplyMarkup buttons=null)
        {
            ChatId = id;
            Latitude = latitude;
            Longitude = longitude;
            Buttons = buttons;
        }
        
        public SenderMessage(long id, string message, bool sendNotification=false, int replyToMessageId=0, int messageId=0)
        {
            ChatId = id;
            Message = message;
            MessageId = messageId;
            SendNotification = sendNotification;
            ReplyToMessageId = replyToMessageId;
        }

        public SenderMessage(long id, string message, IReplyMarkup buttons, bool sendNotification=false, int replyToMessageId=0, int messageId=0) : 
            this(id, message, sendNotification, replyToMessageId, messageId)
        {
            Buttons = buttons;
        }
    }
}