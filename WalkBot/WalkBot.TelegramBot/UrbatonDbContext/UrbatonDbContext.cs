using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;
using Microsoft.EntityFrameworkCore;

namespace WalkBot.TelegramBot.UrbatonDbContext
{
    public class UrbatonDbContext : DbContext
    {
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<UserStateInfo> UserStateInfos { get; set; }

        public UserInfo GetUser(long chatId)
        {
            return UserInfo.Where(x => x.ChatId == chatId).Include(x=>x.CurrentState).Include(x=>x.CurrentState.FilterType).FirstOrDefault();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=Urbaton;Integrated Security=True";
            optionsBuilder.UseInMemoryDatabase("Urbaton");
//            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfo>().HasKey(x => x.UserId);
            base.OnModelCreating(modelBuilder);
        }
    }
}