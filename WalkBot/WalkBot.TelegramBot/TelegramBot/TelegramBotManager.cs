﻿using System;
using System.Collections.Generic;
using System.Net;
using FMS.GSheets.GSheets;
using FMS.GSheets.GSheets.State;
using FMS.Infrastructure.Infrastructure;
using Telegram.Bot;
using Telegram.Bot.Args;
using WalkBot.TelegramBot.Extensions;

namespace WalkBot.TelegramBot.TelegramBot
{
    public class TelegramBotManager
    {
        private readonly SheetApi _sheetApi;
        private readonly TelegramBotClient _client;

        private UsersInformation UsersInformation { get; set; }
        private Collector Collector { get; set; }

        private readonly Dictionary<string, IList<StateInfo>> stateInfos;

        public TelegramBotManager(SheetApi sheetApi, BotInformation info)
        {
            _sheetApi = sheetApi;
            stateInfos = _sheetApi.GetStateInfos();
            var wp = new WebProxy(info.Proxy)
            {
                Credentials = new NetworkCredential(info.Username, info.Password)
            };

            _client = new TelegramBotClient(info.Token, wp);
            UsersInformation = new UsersInformation();
            _client.OnUpdate += OnUpdate;
        }

        public void Run()
        {
            Console.WriteLine("TelegramBotManager - Run");

            _client.StartReceiving();
            while (true)
            {
            }
        }

        private void OnUpdate(object sender, UpdateEventArgs e)
        {
            Console.WriteLine("TelegramBotManager - OnUpdate");
            Collector = new Collector(_client, stateInfos);

            var chatId = e.Update.GetChatId();

            if (chatId == 0) return;


            string stateName = "";
            if (!UsersInformation.ContainsKey(chatId))
            {
                UsersInformation.AddUser(chatId);
            }


            var state = Collector.ReCreateEntities(chatId, UsersInformation);
            if (state == null) return;
            new MessageHandler(state, e.Update).Handle();
        }
    }
}