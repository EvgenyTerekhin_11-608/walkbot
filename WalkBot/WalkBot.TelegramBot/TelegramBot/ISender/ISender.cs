using Telegram.Bot.Types.ReplyMarkups;

namespace WalkBot.TelegramBot.TelegramBot.ISender
{
   public  interface ISender
    {
        void Send(string message, IReplyMarkup buttons=null, int replyToMessageId=0);
        void Send(string message);
        void SendTo(long chatId, string message, IReplyMarkup buttons=null, bool sendNotification=false, int replyToMessageId=0);
        void SendEdit(string message, int messageId, IReplyMarkup buttons = null);
        void EditReply(int messageId, IReplyMarkup markup);
        void SendLocation(float latitude, float longitude);
    }
}