using System.Collections.Generic;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;

namespace WalkBot.TelegramBot.TelegramBot.ISender
{
    public class Sender : ISender
    {
        private readonly long _chatId;
        private readonly Queue<SenderMessage> _queue;

        public Sender(long chatId, Queue<SenderMessage> queue)
        {
            _chatId = chatId;
            _queue = queue;
        }

        public void Send(string message)
        {
            SendTo(_chatId, message);
            //Client.SendTextMessageAsync(ChatId, message, ParseMode.Markdown);
        }

        public void Send(string message, IReplyMarkup buttons, int replyToMessageId=0)
        {
            SendTo(_chatId, message, buttons, false, replyToMessageId);
            //Client.SendTextMessageAsync(ChatId, message, replyMarkup: buttons, parseMode:ParseMode.Markdown);
        }

        public void SendEdit(string message, int messageId, IReplyMarkup buttons)
        {
            _queue.Enqueue(new SenderMessage(_chatId, message, buttons, false, 0, messageId));
        }

        public void EditReply(int messageId, IReplyMarkup markup)
        {
            SendEdit("", messageId, markup);
        }

        public void SendLocation(float latitude, float longitude)
        {
            _queue.Enqueue(new SenderMessage(_chatId, latitude, longitude));
        }

        private void SendTo(long chatId, string message, bool sendNotification=false, int replyToMessageId=0)
        {
            _queue.Enqueue(new SenderMessage(chatId, message, sendNotification, replyToMessageId));
        }

        public void SendTo(long chatId, string message, IReplyMarkup buttons, bool sendNotification=false, int replyToMessageId=0)
        {
            _queue.Enqueue(new SenderMessage(chatId, message, buttons, sendNotification, replyToMessageId));
        }
    }
}