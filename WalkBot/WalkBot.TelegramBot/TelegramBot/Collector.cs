using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FMS.GSheets.GSheets.State;
using FMS.Infrastructure.Infrastructure;
using FMS.Infrastructure.Infrastructure.Storage.Wrappers;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.ISender;
using WalkBot.TelegramBot.TelegramBot.State;

namespace WalkBot.TelegramBot.TelegramBot
{
    public class Collector
    {
        private readonly TelegramBotClient _botClient;
        private Sender Sender { get; set; }
        private UserService UserService { get; set; }
        private ActionStorageWrapper ActionStorageWrapper { get; set; }
        private FMS.Builder.Builder Builder { get; }

        private Dictionary<string, IList<StateInfo>> StateInfos { get; set; }

        private Queue<SenderMessage> MessagesQueue { get; }

        private UrbatonDbContext.UrbatonDbContext _context { get; }

        public Collector(TelegramBotClient botClient, Dictionary<string, IList<StateInfo>> stateInfos)
            : this(botClient, new Queue<SenderMessage>(), new FMS.Builder.Builder())
        {
            StateInfos = stateInfos;
        }

        private Collector(TelegramBotClient botClient, Queue<SenderMessage> messageQueue, FMS.Builder.Builder builder)
        {
            _botClient = botClient;
            MessagesQueue = messageQueue;
            Builder = builder;
            SendMessages(MessagesQueue, _botClient);
            _context = new UrbatonDbContext.UrbatonDbContext();
        }

        private async void SendMessages(Queue<SenderMessage> queue, TelegramBotClient client)
        {
            while (true)
            {
                if (queue.Count == 0)
                {
                    //Console.WriteLine("wait");
                    await Task.Delay(50);
                }
                else
                {
                    for (var i = 0; i < 3; i++)
                    {
                        if (queue.Count > 0)
                            await client.SendMessage(queue.Dequeue());
                    }

                    await Task.Delay(100);
                }
            }
        }

        public State.State ReCreateEntities(long chatId, UsersInformation users)
        {
            if (!_context.UserInfo.Any(x => x.ChatId == chatId))
            {
                _context.UserInfo.Add(new UserInfo(chatId));
                _context.SaveChanges();
            }

            Sender = new Sender(chatId, MessagesQueue);
            UserService = new UserService(chatId);
            ActionStorageWrapper = new ActionStorageWrapper(users.GetData(chatId), UserService);
            var stateName = ActionStorageWrapper.Set<CurrentStateInfo>().Type.ToString();
            var stateBuilder = new StateBuilder(
                stateName,
                Sender,
                ActionStorageWrapper,
                Builder,
                _context);
            return stateBuilder.GetState(StateInfos);
        }
    }
}