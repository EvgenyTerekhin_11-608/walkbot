using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;

namespace WalkBot.TelegramBot.TelegramBot
{
    public class KeyboardBuilder
    {
        public KeyboardBuilder()
        {
        }

        public InlineKeyboardMarkup Build(IEnumerable<string> names)
        {
            var buttons = names.Select(x =>
                x.Contains("-")
                    ? new InlineKeyboardButton() {Text = x, CallbackData = x.Substring(0, x.IndexOf('-')).Trim()}
                    : new InlineKeyboardButton() {Text = x, CallbackData = x}).ToTeil(2).ToList();
            //return new InlineKeyboardMarkup(names.Select(InlineKeyboardButton.WithCallbackData).ToTeil(2).ToList());
            return new InlineKeyboardMarkup(buttons);
        }

        public ReplyKeyboardMarkup BuildReply(IEnumerable<string> names)
        {
            var result = new ReplyKeyboardMarkup(names.Select(x => new KeyboardButton(x)));
            result.OneTimeKeyboard = true;
            return result;
        }
        
        public ReplyKeyboardRemove BuildRemove()
        {
            return new ReplyKeyboardRemove();
        }
    }
}