﻿using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.State
{
    public class State
    {
        private IEnumerable<CommandBase> StateActions { get; }

        public State(IEnumerable<CommandBase> commands)
        {
            StateActions = commands;
        }

        public void Execute(Update update)
        {
            var command = StateActions.First(x =>
                x.Validators.Aggregate(false, (a, c) => a || c.IsValid(update)) &&
                x.ContainsValidator.Aggregate(false, (a, c) => a || c.IsValid(update)) &&//.isValid(update) &&
                x.UseMeCommands.Aggregate(true, (a, c) => a && c.Check(update)));

            command.ExecuteCommands(update);
            command.ChangeState();
        }
    }
}