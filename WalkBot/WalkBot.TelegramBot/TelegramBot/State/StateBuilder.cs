using System;
using System.Collections.Generic;
using System.Linq;
using FMS.GSheets.GSheets.State;
using FMS.Infrastructure.Infrastructure.Storage.Wrappers;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;
using WalkBot.TelegramBot.TelegramBot.ISender;
using WalkBot.TelegramBot.TelegramBot.Validators;
using WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators;

namespace WalkBot.TelegramBot.TelegramBot.State
{
    public class StateBuilder
    {
        //private readonly SheetApi _sheetApi;
        //private readonly Dictionary<string, IList<StateInfo>> _stateInfos;
        private readonly ActionStorageWrapper _actionStorageWrapper;
        private readonly FMS.Builder.Builder _builder;
        private readonly UrbatonDbContext.UrbatonDbContext _context;
        private string StateName { get; }


        private readonly List<object> _useMeParameters;
        private readonly List<object> _commandParameters;

        public StateBuilder(
            //Dictionary<string, IList<StateInfo>> stateInfos,
            string stateName,
            Sender sender,
            ActionStorageWrapper actionStorageWrapper,
            FMS.Builder.Builder builder,
            UrbatonDbContext.UrbatonDbContext context)
        {
            StateName = stateName;
            //_stateInfos = stateInfos;
            _actionStorageWrapper = actionStorageWrapper;
            _builder = builder;
            _context = context;
            _useMeParameters = new List<object>
            {
                 context
            };
            _commandParameters = new List<object>
            {
                 sender, context
            };
        }

        public State GetState(Dictionary<string, IList<StateInfo>> _stateInfos)
        {
            var stateInfos = _stateInfos[StateName];
            var commandBases = stateInfos.Select(BuildCommand).ToList();
            var state = new State(commandBases);
            return state;
        }

        CommandBase BuildCommand(StateInfo info)
        {
            var useMe = UseMeBuild(info.UseMeCheckers);
            var command = CommandBuild(info.Commands);
            var transaction = TransactionManagerBuild(info.FromToState);
            var validators = ValidatorsBuild(info.Validators);
            var containsValidator = ContainsValidatorBuild(info.ContainsMessage);
            return new CommandBase(useMe, command, validators, containsValidator, transaction);
        }

        List<ContainsValidator> ContainsValidatorBuild(List<string> contains)
        {
            var result = contains
                .Select(x => new ContainsValidator(x)).ToList();
            return result;
        }

        List<PredicateValidator> ValidatorsBuild(List<string> validators)
        {
            var result = validators.Select(x => _builder.Build(x, new List<object>())).Cast<PredicateValidator>()
                .ToList();
            return result;
        }

        List<BaseClasses.UseMe> UseMeBuild(List<string> useMe)
        {
            var result = useMe.Select(x => _builder.Build(x, _useMeParameters)).Cast<BaseClasses.UseMe>().ToList();
            return result;
        }

        List<Command> CommandBuild(List<string> commands)
        {
            var enumerable = commands.Select(x => _builder.Build(x, _commandParameters));
            var cast = enumerable.Cast<Command>();
            var result = cast.ToList();
            return result;
        }

        TransactionManager TransactionManagerBuild(string state)
        {
            return new TransactionManager(_context,Enum.Parse<StateType>(state), _actionStorageWrapper);
        }
    }
}