using Telegram.Bot.Types;
using WalkBot.TelegramBot.TelegramBot.ISender;

namespace WalkBot.TelegramBot.TelegramBot.BaseClasses
{
    public abstract class Command
    {
        protected readonly ISender.ISender Sender;
        protected readonly UrbatonDbContext.UrbatonDbContext _context;
        protected EnumFilter EnumFilter { get; } = new EnumFilter();

        protected Command(
            ISender.ISender sender,
            UrbatonDbContext.UrbatonDbContext context
        )
        {
            Sender = sender;
            _context = context;
        }

        public abstract void Execute(Update update);
    }
}