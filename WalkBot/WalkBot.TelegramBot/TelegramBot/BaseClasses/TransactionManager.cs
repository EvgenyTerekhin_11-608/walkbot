using FMS.GSheets.GSheets.State;
using FMS.Infrastructure.Infrastructure.Storage.Wrappers;
using FMS.Infrastructure.Infrastructure.UserService;

namespace WalkBot.TelegramBot.TelegramBot.BaseClasses
{
    public class TransactionManager
    {
        private readonly UrbatonDbContext.UrbatonDbContext _context;
        private readonly StateType _stateType;
        private readonly ActionStorageWrapper _wrapper;

        public TransactionManager(
            UrbatonDbContext.UrbatonDbContext context,
            StateType stateType,
            ActionStorageWrapper wrapper)
        {
            _context = context;
            _stateType = stateType;
            _wrapper = wrapper;
        }

        public void ChangeState()
        {
            _wrapper.Set<CurrentStateInfo>().ChangeState(_stateType);
            _context.SaveChanges();    
        }
    }
}