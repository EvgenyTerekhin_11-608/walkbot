﻿using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.TelegramBot.UseMe
{
    public class True: BaseClasses.UseMe
    {

        public override bool Check(Update update) => true;


        public True(UrbatonDbContext.UrbatonDbContext context) : base(context)
        {
        }
    }
}