using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.TelegramBot.Validators
{
    public interface IValidator
    {
        bool IsValid(Update update);
    }
}