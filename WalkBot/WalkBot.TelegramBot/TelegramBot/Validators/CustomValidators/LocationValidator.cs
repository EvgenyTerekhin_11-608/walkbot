namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class LocationValidator: PredicateValidator
    {
        public LocationValidator() : base(x=>x.Message?.Location!=null)
        {
        }
    }
}