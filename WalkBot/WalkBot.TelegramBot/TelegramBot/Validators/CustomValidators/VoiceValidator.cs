namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class VoiceValidator : PredicateValidator
        {
            public VoiceValidator() : base(x => x.Message?.Voice != null)
            {
            }
        }
}