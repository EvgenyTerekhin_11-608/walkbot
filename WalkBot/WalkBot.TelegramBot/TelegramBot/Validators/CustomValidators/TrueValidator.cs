namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class TrueValidator:PredicateValidator
    {
        public TrueValidator() : base(x=>true)
        {
        }
    }
}