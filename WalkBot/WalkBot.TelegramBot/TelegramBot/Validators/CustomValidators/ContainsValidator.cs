using System;

namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class ContainsValidator : PredicateValidator
    {
        public ContainsValidator(string str) : base(x =>
        {
            if (str == String.Empty)
                return true;
            var message = x.Message?.Text ?? x.CallbackQuery?.Data;
            return message?.StartsWith(str) ?? false;
        })
        {
        }
    }
}