namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class CaptionValidator: PredicateValidator
    {
        public CaptionValidator() : base(x => x?.Message?.Caption != null)
        {
        }
    }
}