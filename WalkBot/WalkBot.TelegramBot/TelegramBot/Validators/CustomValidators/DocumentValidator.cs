namespace WalkBot.TelegramBot.TelegramBot.Validators.CustomValidators
{
    public class DocumentValidator: PredicateValidator
    {
        public DocumentValidator() : base(x => x?.Message?.Document != null || x?.Message?.Photo != null)
        {
        }
    }
}