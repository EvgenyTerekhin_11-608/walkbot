using System;
using Telegram.Bot.Types;

namespace WalkBot.TelegramBot.TelegramBot.Validators
{
    public class PredicateValidator : IValidator
    {
        private readonly Func<Update, bool> _predicate;

        public PredicateValidator(Func<Update, bool> predicate)
        {
            _predicate = predicate;
        }

        public bool IsValid(Update update)
        {
            var result = _predicate(update);
            return result;
        }
    }
}