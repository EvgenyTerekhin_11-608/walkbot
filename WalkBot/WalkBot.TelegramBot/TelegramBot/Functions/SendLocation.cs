using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using FMS.Infrastructure.Infrastructure;
using FMS.Infrastructure.Infrastructure.UserService;
using Newtonsoft.Json;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class SendLocation : Command
    {
        public SendLocation(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var userState = _context.GetUser(update.GetChatId()).CurrentState;

            var httpClient = new HttpClient();

            var request = new HttpRequestMessage(HttpMethod.Post,
                $"http://91.240.84.177:5000");

            string userCost;

            switch (userState.Cost)
            {
                case Cost.Low:
                    userCost = "cheap";
                    break;
                case Cost.High:
                    userCost = "expensive";
                    break;
                case Cost.Medium:
                    userCost = new Random().Next(1) == 0 ? "cheap" : "expensive";
                    break;
                case Cost.Free:
                case Cost.UnLimited:
                default:
                    userCost = "";
                    break;
            }

            var filter = userState.FilterType.Select(x => x.Type.GetDisplayEnum());

            var anonymous = JsonConvert.SerializeObject(new
                {lat = userState.Latitude, lng = userState.Longitude, filters = filter, count = 4});

            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json")); //ACCEPT header

            request.Content = new StringContent(anonymous,
                Encoding.Default,
                "application/json");

            var response = httpClient.SendAsync(request).Result;

            var iamContent = response.Content.ReadAsStringAsync().Result.Substring(1);
            iamContent = iamContent.Substring(0, iamContent.Length - 2);

            iamContent = iamContent.Replace("\\\"", "\"").Replace("\\\\", "\\");

            var iamDefinition = new
                {name = "", location_lat = (float) 0, location_lng = (float) 0, rating = (float) 0, address = ""};

            var list = JsonConvert.DeserializeObject<Place[]>(iamContent).ToList();

            Place loc = null;
            if (list.Count > 0)
                loc = list[new Random().Next(list.Count-1)];

            if (loc != null)
            {
                Sender.Send($"{loc.name}, {loc.address}");
                Sender.SendLocation(loc.location_lat, loc.location_lng);
                Sender.Send("Хотите ли вы продолжить прогулку?", new ReplyKeyboardMarkup(new List<KeyboardButton>
                {
                    new KeyboardButton("Продолжить") {RequestLocation = true},
                    new KeyboardButton("Закончить")
                }.ToTeil(2)) {ResizeKeyboard = true});
            }
            else
            {
                Sender.Send("По выбранным фильтрам не найдено мест. Хотите ли вы повторить?", new ReplyKeyboardMarkup(
                    new List<KeyboardButton>
                    {
                        new KeyboardButton("Повторить") {RequestLocation = true},
                        new KeyboardButton("Закончить")
                    }.ToTeil(2)) {ResizeKeyboard = true});
            }
        }
    }
}