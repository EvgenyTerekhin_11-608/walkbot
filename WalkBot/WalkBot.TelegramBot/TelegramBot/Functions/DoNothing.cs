using Telegram.Bot.Types;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class DoNothing: Command
    {
        public DoNothing(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            // Do nothing
        }
    }
}