using System.Collections.Generic;
using System.Linq;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChooseRelaxType: Command
    {
        public ChooseRelaxType(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var buttons = new List<string>
            {
                "Активный",
                "Спокойный"
            };

            var keyboard = buttons.Select((x, i) => new InlineKeyboardButton {Text = x, CallbackData = i.ToString()})
                .ToTeil(2);
            if(update.CallbackQuery!=null)
                Sender.SendEdit("Выберите тип отдыха", update.CallbackQuery.Message.MessageId, new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите тип отдыха", new InlineKeyboardMarkup(keyboard));
        }
    }
}