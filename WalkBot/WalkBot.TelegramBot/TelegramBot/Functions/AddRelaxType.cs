using System;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class AddRelaxType: Command
    {
        public AddRelaxType(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if(user!=null)
                user.CurrentState.RelaxationType =
                    (RelaxationType) Convert.ToInt32(update.GetMessage());
        }
    }
}