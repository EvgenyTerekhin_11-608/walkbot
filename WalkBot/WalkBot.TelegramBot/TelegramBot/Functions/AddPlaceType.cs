using System;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class AddPlaceType: Command
    {
        public AddPlaceType(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if(user!=null)
                user.CurrentState.PlaceType =
                    (PlaceType) Convert.ToInt32(update.GetMessage());
        }
    }
}