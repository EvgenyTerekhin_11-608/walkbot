using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class Authorize: Command
    {
       
        public override void Execute(Update update)
        {
            Sender.Send("Вас приветствует WalkBot. Нажмите  <b>Начать прогулку</b>  для начала", 
                new ReplyKeyboardMarkup(new KeyboardButton("Начать прогулку"){RequestLocation = true}){OneTimeKeyboard = true, ResizeKeyboard = true});
        }

        public Authorize(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }
    }
}