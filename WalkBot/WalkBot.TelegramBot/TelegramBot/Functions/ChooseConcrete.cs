using System.Collections.Generic;
using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ChooseConcrete: Command
    {
        public ChooseConcrete(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var state = _context.GetUser(update.GetChatId()).CurrentState;
            var hidden = EnumFilter.GetHidden<PlaceType,FilterType>(
                state.PlaceType);

            var keyboard = hidden.Format().ToList();

            var message = "";
            if (state.FilterType.Count > 0)
            {
                message += "Выбранные: ";
                keyboard.Add(new List<InlineKeyboardButton>{InlineKeyboardButton.WithCallbackData("Продолжить")});
            }

            foreach (var filter in state.FilterType)
                message += $"{filter.Type.GetDisplayNormal()} ";

            message += "\n\n";
            
            if (update.CallbackQuery != null)
                Sender.SendEdit(message+"Выберите конкретное место", update.CallbackQuery.Message.MessageId,
                    new InlineKeyboardMarkup(keyboard));
            else
                Sender.Send("Выберите конкретное место", new InlineKeyboardMarkup(keyboard));
        }
    }
}