using System.Collections.Generic;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class ClearFilters: Command
    {
        public ClearFilters(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if (user != null)
            {
                user.CurrentState.FilterType = new List<FilterTypeInfo>();
                _context.SaveChanges();
            }
        }
    }
}