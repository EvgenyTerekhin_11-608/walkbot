using System;
using System.Linq;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class AddConcreteType: Command
    {
        public AddConcreteType(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if (user != null)
            {
                var filter = (FilterType) Convert.ToInt32(update.GetMessage());
                if (user.CurrentState.FilterType.Any(x=>x.Type==filter))
                    user.CurrentState.FilterType.Remove(user.CurrentState.FilterType.FirstOrDefault(x=>x.Type==filter));
                else
                {
                    user.CurrentState.FilterType.Add(new FilterTypeInfo() {Type = filter});
                    _context.SaveChanges();
                }
            }
        }
    }
}