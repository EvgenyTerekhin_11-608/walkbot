using System;
using FMS.Infrastructure.Infrastructure.UserService;
using Telegram.Bot.Types;
using WalkBot.TelegramBot.Extensions;
using WalkBot.TelegramBot.TelegramBot.BaseClasses;

namespace WalkBot.TelegramBot.TelegramBot.Functions
{
    public class AddCost: Command
    {
        public AddCost(ISender.ISender sender, UrbatonDbContext.UrbatonDbContext context) : base(sender, context)
        {
        }

        public override void Execute(Update update)
        {
            var user = _context.GetUser(update.GetChatId());
            if(user!=null)
                user.CurrentState.Cost =
                    (Cost) Convert.ToInt32(update.GetMessage());
        }
    }
}