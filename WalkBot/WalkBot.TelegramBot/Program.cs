﻿using System;
using FMS.GSheets.GSheets;
using FMS.Infrastructure.Infrastructure;
using Microsoft.Extensions.Configuration;
using WalkBot.TelegramBot.TelegramBot;

namespace WalkBot.TelegramBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(new DirectoryCreator().ToString())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentName}.json", true, true);
            
            var configuration = builder.Build();
            
            var botStatesId = configuration.GetSection("GoogleSheets:BotStates:Id").Value;
            var certPath = configuration.GetSection("GoogleSheets:ServiceAccountCertificatePath").Value;
            var certPassword = configuration.GetSection("GoogleSheets:ServiceAccountCertificatePassword").Value;
            var serviceAccountEmail = configuration.GetSection("GoogleSheets:ServiceAccountEmail").Value;
            
            SheetApi api = new SheetApi(botStatesId, "A2:E", serviceAccountEmail, certPath, certPassword);

            var botInfo = configuration.GetSection("BotInformation").Get<BotInformation>();

            //var whitelistedLists = configuration.GetSection("WhiteListedLists").Get<List<string>>();
            
            var TimeZoneId = configuration.GetSection("TimeZoneId").Value;
            if (TimeZoneId != null)
            {
                Environment.SetEnvironmentVariable("Time_Zone_Id", TimeZoneId);
            }
            
            var tbm = new TelegramBotManager(api, botInfo);
            tbm.Run();
        }
    }
}