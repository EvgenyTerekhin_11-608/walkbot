namespace FMS.GSheets.GSheets.State
{
    public enum StateType
    {
        UnAuth,
        Start,
        Age,
        Cost,
        RelaxType,
        PlaceType,
        ConcretePlace,
        Location
    }
}