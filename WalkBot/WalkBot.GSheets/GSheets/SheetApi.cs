using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using FMS.GSheets.GSheets.State;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace FMS.GSheets.GSheets
{
    public class SheetApi
    {
        public string ServiceAccountEmail { get; }
        public string ServiceAccountCertificatePath { get; }
        public string ServiceAccountCertificatePassword { get; }


        private const string ApplicationName = "WalkBot";

        public IList<IList<Object>> Values { get; private set; }
        protected SheetsService Service { get; set; }

        public Range CurrentRange { get; private set; }
        
        public string Id { get; private set; }

        public void SetId(string id)
        {
            Id = id;
        }
        
        public void SetRange(string newRange)
        {
            CurrentRange = new Range(newRange);
        }

        #region Конструкторы

        protected SheetApi(string id, string serviceAccountEmail, string serviceAccountCertificatePath,
            string serviceAccountCertificatePassword)
        {
            Id = id;

            ServiceAccountEmail = serviceAccountEmail;
            ServiceAccountCertificatePath = serviceAccountCertificatePath;
            ServiceAccountCertificatePassword = serviceAccountCertificatePassword;

            Authorize();
        }

        public SheetApi(string id, string range, string serviceAccountEmail, string serviceAccountCertificatePath,
            string serviceAccountCertificatePassword) : this(id, serviceAccountEmail, serviceAccountCertificatePath,
            serviceAccountCertificatePassword)
        {
            CurrentRange = new Range(range);
        }

        #endregion

        public override string ToString()
        {
            var s = new StringBuilder();
            foreach (var value in Values)
            {
                foreach (var val in value)
                {
                    s.Append($"{val} | ");
                }

                s.Append("\n\n");
            }

            return s.ToString();
        }


        private void Authorize()
        {
            var directory = Environment.CurrentDirectory;
               
            var certificate = new X509Certificate2(directory + "/" + ServiceAccountCertificatePath,
               ServiceAccountCertificatePassword, X509KeyStorageFlags.Exportable);
            
            var credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(ServiceAccountEmail)
               {
                   Scopes = new[] {SheetsService.Scope.Spreadsheets}
               }.FromCertificate(certificate));
            
            // Create the service
            Service = new SheetsService(new BaseClientService.Initializer()
            {
               HttpClientInitializer = credential,
               ApplicationName = ApplicationName,
            });
        }

        #region Получение данных

        public async Task<Spreadsheet> Get(string fields)
        {
            var req = Service.Spreadsheets.Get(Id);
            req.Fields = fields;

            var sheet = await req.ExecuteAsync();
            return sheet;
        }
        
        public async Task GetValues()
        {
            var request = Service.Spreadsheets.Values.Get(Id, CurrentRange.ToString());
            
            var response = await request.ExecuteAsync();
            
            Values = response.Values;
        }

        public async Task<IList<IList<object>>> GetValues(string range)
        {
            CurrentRange = new Range(range);

            await GetValues();
            return Values;
        }

        protected StateInfo ToStateInfo(IList<object> sheet) => new StateInfo
        {
            UseMeCheckers = sheet[0].ToString().Split(',').ToList(),
            Validators = sheet[1]?.ToString()?.Split(',')?.ToList(),
            ContainsMessage = sheet[2].ToString().Split(',').ToList(),
            /*sheet.Count < 5
            ? ""
            : $"{sheet[2]}",*/
            Commands = sheet[3].ToString().Split(',').ToList(),
            FromToState = $"{sheet[4]}"
        };
        
        public Dictionary<string, IList<StateInfo>> GetStateInfos()
        {
           var SheetsDictionary = DownloadData(
                Service.Spreadsheets.Get(Id)
                    .Execute()
                    .Sheets
                    .Select(x => x.Properties.Title)
                    .ToList()
                ).Result;

            var result = new Dictionary<string, IList<StateInfo>>();
            foreach (var value in SheetsDictionary)
            {
                result.Add(value.Key, value.Value.Select(ToStateInfo).ToList());
            }
            
            return result;
        }
        
        private async Task<Dictionary<string, IList<IList<object>>>> DownloadData(IEnumerable<string> titles)
        {
            var dic = new Dictionary<string, IList<IList<object>>>();
            Console.WriteLine("Start data loading for state machine operation");
            foreach (var t in titles)
            {
                CurrentRange.SheetName = t;
                //var range = $"{t}!A2:E";
                var data = Service.Spreadsheets.Values.Get(Id, CurrentRange.ToString()).ExecuteAsync();
                dic.Add(t, (await data).Values);
            }

            Console.WriteLine("End");
            return dic;
        }
        
        

        #endregion

        #region Запросы на изменение

        // Перезаписать значения строк в таблице
        public async Task<bool> UpdateValues()
        {
            var valueRange = new ValueRange();
            valueRange.Values = Values;

            var request = Service.Spreadsheets.Values.Update(valueRange, Id, CurrentRange.ToString());
            request.ValueInputOption =
                SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;

            await request.ExecuteAsync();
            return true;
        }

        public async Task ClearRange()
        {
            Values = new List<IList<object>>();
            
            var request = Service.Spreadsheets.Values.Clear(new ClearValuesRequest(), Id, CurrentRange.ToString());
            
            await request.ExecuteAsync();
        }
        
        // Перезаписать значения строки в таблице
        public async Task<bool> UpdateValue(int rowNumber)
        {
            var valueRange = new ValueRange();
            valueRange.Values = Values;
            CurrentRange.FromNumber += rowNumber;
            CurrentRange.ToNumber = CurrentRange.FromNumber;
            var request = Service.Spreadsheets.Values.Update(valueRange, Id, CurrentRange.ToString());
            request.ValueInputOption =
                SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;

            await request.ExecuteAsync();
            return true;
        }

        // Добавить строки в конец таблицы
        public async Task AppendValues()
        {
            var valueRange = new ValueRange();
            valueRange.Values = Values;
            //valueRange.Range = CurrentRange.ToString();

            var request = Service.Spreadsheets.Values.Append(valueRange, Id, CurrentRange.ToString());
            request.ValueInputOption =
                SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

            await request.ExecuteAsync();
            //Console.WriteLine(response.TableRange);
        }

        #endregion

        #region Работа со значениями таблицы

        // Изменение всех данных
        public void ChangeValues(IList<IList<object>> list)
        {
            Values = list;
        }
        
        // Добавить строку в массив values
        public void AddValue(int index, IList<Object> value)
        {
            Values.Insert(index, value);
            if(CurrentRange.ToNumber!=0)
                CurrentRange.ToNumber++;
        }

        // Получить строку из массива
        public IList<Object> GetValue(int index)
        {
            if (index < Values.Count)
                return Values[index];
            else throw new IndexOutOfRangeException();
        }

        // Изменить строку в массиве
        public void SetValue(int index, IList<Object> value)
        {
            Values[index] = value;
        }

        #endregion

    }
}